# Flat Mount Rear Panel

This is a new system for the rear panel of the electronics box. It uses the slot in the upper 2020 extrusion to secure the upper portion of the panel pieces in place while using screws to secure the lower portion of the panel to the outer portion of the lower 2020 extrusion. This solution is much easier to install, forgiving of tolerances in the gap between the 2020s, and is much less likely to break.

**Included are:**

* 2x variants of power panel. The kit one is modded to work with both the glue in or clip in power switches per @readeral's spec.
* 4x variants of wiring / usb panels.
* 8x variants of vents, to give you flexibility in layout based on your needs
* End caps - Clips into the 2080s to finish the look of the panel

## Printing Notes:

Recommend printing with support for the overhang created by the slot that goes into the extrusion. The tolerance on this piece is exact, so you may need to play with flow / scaling to get it to fit in the slot. I found that pushing down on the slot piece against the edge of a table on the face that touched the supports, can help to ensure that section is flat and make it easier to fit into the extrusion.

![Example Layout](https://bitbucket.org/malhan-designs/x301-mods/raw/d13efc3c5fa5128621a22e25e8af4c051bc5d65a/Flat%20Mount%20Rear%20Panel/example-layout.png)
