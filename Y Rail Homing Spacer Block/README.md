# Y Rail Spacer Block

I use this simple model as an endstop for the auto homing on the Y axis. I place these over the Y rail behind the stepper motor mounts. This allows my Y homing to be located where I want it without any offsets in Marlin.

One side is filleted to fit tightly against the back side of my Braced XY Motor Brackets.

There are two length options included:
* 19mm - Best for use with the standard LF XY Brackets
* 25mm - Best for use with my composite XY Brackets

## Printing Notes:

* You can reduce the offset if you place the side that isn't filleted on the build plate and give the object a negative Z position in cura, thus tuning it to your desired home point.
* Print 2, one for each side. 
* A couple dabs of super glue on the face that touches the motor brackets will help keep them quiet during homing.

![Y Rail Spacer Block](https://bitbucket.org/malhan-designs/x301-mods/raw/ba67a641692190dc9dcbf6f925c5ceecd7635b56/Y%20Rail%20Homing%20Spacer%20Block/model.png)