# Spool Holder - 608 Bearing

This model reduces friction on the spool holder for the X301. It uses 608 Bearings (skateboard bearings) with pins to hold them in place. The spool rolls on the 608 bearings during printing, thus easing the work of the extruder motor.

Derived from: https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Spool%20Mount.f3d

## Additional parts:

* 2x 608 Bearings - These are easy to source on Amazon. I personally use ABEC 608ZZ bearings.

## Changes from Original Design:

* Adds channels for the 608 bearings and pins.
* Adds a lip to the back side of the mount to encourage the spool to stay located on the bearings.
* Increases the height and width of the mount to offset the loss of mass in the center of the mount.
* Adds the LayerFused logo to the mount.

## Printing Notes:

* Print 1 Spool mount and 2 pins
* I recommend printing the mount horizontally, in the orientation you would install it. This requires supports. Cura will create nice clean supports that are easily removed. By doing it this way, you won't need supports in the bearing channels, which is important.

![Spool Holder - 608 Bearing](https://bitbucket.org/malhan-designs/x301-mods/raw/461a89d6b79e2b997bd4ada6232bc157c86d8aa9/Spool%20Holder%20-%20608%20Bearing/model.png)