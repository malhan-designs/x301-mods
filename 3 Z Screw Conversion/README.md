# 3 Z Screw Conversion

These models aid in the conversion to a 3 lead screw setup for the Z axis. This conversion is a complex process for the builder who is not afraid of thinking outside the box and is comfortable with reconfiguring firmware.

Requires the frame from the 6 + 2 Point Heated Bed Frame, **but using the STL files included here**. The rear sides and front center mounts are reused, the rear center and front sides are new. **Ensure you have the required hardware from that mod before attempting this!**

## Additional parts:

**Sized according to which X printer you're building (x301 = 400mm):**

* 1x 2020 extrusion

**Matched to whatever you're using for the 2 existing screws:**

* 1x Lead screw
* 1x Lead screw to stepper coupler
* 1x MGN12H Linear rail and block
* 1x Nema 17 stepper
* M3 and M5 Hardware to attach these things to the chassis

**Various Bracketry to hold the 2020 extrusion in place, your choice:**

* 4x 90* inner corner brackets
* 2x M5 T outer brackets

**If you use the 20mm option for the 2040s, recommended**

* 4x 20mm 90* inner corner brackets

**Electronics**

* 1x additional stepper wire
* 1x or 2x TMC2225 / TMC2208 stepper driver, whatever matches your existing stepper drivers (note you need 3x total drivers for this, a stock X301 has 1x driver for the Z)
* Motherboard upgrade to support 6x total stepper drivers - You have multiple options here:
    * BTT EXP MOT - Expansion board for SKR 1.4 turbo to add more drivers, requires additional power wires from PSU, and restricts TFT to touch mode only. Not recommended.
    * BTT SKR PRO
    * BTT SKR GTR

**Acrylic Cover**

* If you want an acrylic cover, I've included DXF files to enable you to get one made, see notes below.

## Printing Notes:

The stepper motor mounts can be printed without supports. The new bed mounts that connect to the linear rail blocks will require supports to print correctly. I print the front mounts standing up, and the rear mount face down.

The new XY Motor Brackets are optional, depending on if you run the 2040s at 20mm or 40mm from the front.

## Assembly Notes:

There are 2 options for moving the 2040 extrusion forward to accomodate the front Z screw positioning:

* **20mm from the front of the chassis** - Install the linear guide in the back channel of the 2040, this requires the modified XY Motor Brackets included here to gain clearance for the side angle brackets to support the 2040.
* **40mm from the front of the chassis** - Install the linear guide in the front channel of the 2040, this option is good if you have already printed out my other XY Motor Brackets and don't want to reprint them. You can always change your mind later and move the extrusion forward if you reprint the XY brackets.

This change will require firmware updates to support the 3 Z drivers. I had to use the bugfix version of marlin to gain support for the EXP MOT's additional pins. This may not be required if using the SKR PRO or GTR.

I've included DXFs for the acrylic cover. I made versions for the x201 and x301. I made 2 variants of each, with and without the front & center wire cutout. I have my wires for the screen coming through the area that the right front Z uses so I have no need for the center cutout.

### Simplified Assembly Steps:

1. Remove your bed and the stock Z mounts from the screws & stepper motors
2. Make space in your electronics box (I just removed everything)
3. Move your 2040s forward
4. Add the rear center 2020
5. Mount the front Z mounts, motors, and screws
6. Mount your rear center rail, Z motor and screw
7. Add the Z bed mounts to the screws,
8. Set the bed on the platform created by the 3 Z bed mounts. 
9. Attach the bed with appropriate hardware.
10. Rewire all the things...
11. Configure firmware and flash motherboard.

![3 Z Screw Conversion](https://bitbucket.org/malhan-designs/x301-mods/raw/d3a1889359e273222d8f954aa57e9eb471146cc6/3%20Z%20Screw%20Conversion/complete-model-a.png)
![3 Z Screw Conversion](https://bitbucket.org/malhan-designs/x301-mods/raw/d3a1889359e273222d8f954aa57e9eb471146cc6/3%20Z%20Screw%20Conversion/complete-model-b.png)
![3 Z Screw Conversion](https://bitbucket.org/malhan-designs/x301-mods/raw/d3a1889359e273222d8f954aa57e9eb471146cc6/3%20Z%20Screw%20Conversion/complete-model-c.png)