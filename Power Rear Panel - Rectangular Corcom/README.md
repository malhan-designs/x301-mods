# Power Rear Panel - Corcom

This model allows the use of a high end, rectangular, TE Corcom filtered power switch in the X301 back panel.

Derived from: https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Power%20Rear%20Panel.f3d

## Additional parts:

* Switch: https://www.digikey.com/en/products/detail/te-connectivity-corcom-filters/4-6609106-6/759349 (Any switch with the same dimensions / retention mechanism should work)
* 2x 6mm x 30mm 10amp glass fuses: https://www.digikey.com/en/products/detail/nte-electronics-inc/74-6SG10A/11647959 (can get them cheaper on Amazon)

## Changes from Original Design:

* Sized to fit a TE Corcom PS0SXDHX0 power switch.
* Adds detents to support the pressure clips on the sides
* Removes the upper screws and replaces them with bumps that self locate into the extrusion

## Printing Notes:

* Print face down
* Tolerances are tight, get your flow and horizontal expansion dialed in

![Power Rear Panel - Corcom](https://bitbucket.org/malhan-designs/x301-mods/raw/702ddfcb9a9b963a85085317fb872dc6415b8319/Power%20Rear%20Panel%20-%20Rectangular%20Corcom/model.png)
