# BTT EXP MOT V1.0 Mount

This mount is for the Big Tree Tech EXP MOT V1.0 expansion board for the SKR 1.3, 1.4, and 1.4 turbo.

![BTT EXP MOT V1.0 Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/5b55e9dad49215281434af3213776e74c65ea869/BTT%20EXP%20MOT%20Mount/model.png)