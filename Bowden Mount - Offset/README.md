# Bowden Mount - Offset

Derived from: 

* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Bowden%20Mount.f3d

This mount is offset in 2 dimensions from the stock mount. It aligns the entry point of the filament more centrally on the filament roll while also bringing the entry point closer to the filament roll.

The screw holes have been widened to work better with M5 screws, and the countersunk portion is compatible with button head screws if desired.

The primary purpose of this is to bring the filament entry point closer to the roll to make up some of the distance created by switching to the 3 Z Screw setup, but it should also work on a stock X301.

![Offset Bowden Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/39f6713a370ff467e375349b5129df04b1639240/Bowden%20Mount%20-%20Offset/model.png)

