# XY Motor Brackets - Braced

These XY Motor Brackets are much more rigid than the standard X301 models. They also reduce strain on the left stepper motor.

Derived from: https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/XY%20Motor%20Brackets.f3d

## Changes from Original Design:

* Adds width to each bracket on the inner side.
* Adds a large gusset on the outside and a small gusset on the inside (for Y travel clearance).
* Adds length to the extrusion mount arms to support the gussets.
* Increases the height of the left motor bracket to bring the stepper shaft pulley in line with the left belt horizontally. This allows the pulley to be mounted upside down, thus moving the torque / pressure from the belt closer to the bearings of the motor. 

## Printing Notes:

* Refer to the LayerFused Part Printing Guide for the correct orientation to print these: https://wiki.layerfused.com/en/X301/printing-guide

![XY Motor Brackets - Braced](https://bitbucket.org/malhan-designs/x301-mods/raw/c69294db75b7768ec3a73b48c7ffc9fbddf69a0f/XY%20Motor%20Brackets%20-%20Braced/model.png)