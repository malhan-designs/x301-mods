# TFT35 E3 V3.0 Mount

This model allows the use of a BTT TFT35 E3 V3.0 touch screen LCD on the X301.

## Hardware Required

* BTT TFT35 E3 V3.0
* 2x M5 x 16mm socket head cap screws / M5 t-nuts to attach the mount to the extrusion

## Printing Notes:

* No supports
* Print face down

![TFT35 E3 V3.0 Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/79825530c443feb0694568b5f4cafba3d69130ef/TFT35%20E3%20V3%20Mount/model.png)
